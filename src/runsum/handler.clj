(ns runsum.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.handler :refer :all]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [selmer.parser :refer :all :as parser]
            [clj-http.client :as client]
            [clojure.data.json :as json]
            [runsum.views :as views])
  (:use runsum.core))

(def strava-settings 
  {:client_id "16739"
   :client_secret "606e2676de98b92a4f61bbefdbbe478d1ce25891"
   :access_token "cddc5095136d37263296c6b1219dcbb59e57c6c0"
   :redirect_uri "http://localhost:3000/"})

(defn get_access_token
  "Requests an access token from Strava using an access code"
  [code]
  (:body (client/post "https://www.strava.com/oauth/token" {:form-params 
                                                            {:client_id (:client_id strava-settings)
                                                             :client_secret (:client_secret strava-settings)
                                                             :code code}})))

(defroutes app-routes
  (GET "/" {params :query-params session :session}
       (println "Session data is: " session)
       (if-not (nil? (get params "code"))
         (do
           (def athlete (json/read-str 
                         (get_access_token (get params "code"))
                         :key-fn keyword))
           (swap! global_athlete conj athlete)
           (println (str "Access token is " (:access_token athlete)))
           (def new_session (assoc session :access_token (:access_token athlete))))
         )
       (def page-settings (assoc strava-settings :access_code (get params "code")))
       {:headers {"Content-Type" "text/html"}
        :body (render-file "base.html" page-settings)
        :session (if (bound? #'new_session)
                   new_session
                   session)})
  (GET "/activities" request (views/get_athlete_activities request))
  ; (GET "/deauthorize" (client/post 
  ;                       "https://strava.com/oauth/deauthorize" 
  ;                       {:oauth-token (:access_token @global_athlete)}))
  (route/not-found "Not Found"))

(def handler (compojure.handler/api app-routes))

(def app
  (wrap-defaults handler site-defaults))
