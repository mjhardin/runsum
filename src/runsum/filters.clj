(ns runsum.filters
   (require [selmer.filters :refer :all]))

(add-filter! :to-miles (fn [m] (format "%.2f" (* 0.62137 (/ m 1000)))))
(add-filter! :to-minutes-seconds (fn [s]
                               (if (> s 3600)
                                 (do
                                   (def seconds (mod s 60))
                                   (def minutes (mod (int (/ s 60)) 60))
                                   (def hours (int (/ s 3600)))
                                   (format "%d:%02d:%02d" hours minutes seconds))
                                 (do
                                   (def seconds (mod s 60))
                                   (def minutes (int (/ s 60)))
                                   (format "%d:%02d" minutes seconds)
                                   ))))
(add-filter! :get-workout-type (fn [t]
                                 (if (some? t)
                                   (get
                                    {:0 "" 
                                     :1 "race"
                                     :2 "long run"
                                     :3 "workout"} (keyword t)))))