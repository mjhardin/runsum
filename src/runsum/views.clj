(ns runsum.views
  (:require [selmer.parser :refer :all :as parser]
            [clojure.data.json :as json]
            [runsum.filters :refer :all])
  (:use runsum.core))

(defn get_athlete_activities
  [request]
  (def access_token (get-in request [:session :access_token]))
  (def activities (json/read-str (:body (clj-http.client/get "https://www.strava.com/api/v3/athlete/activities"
                                                                           {:oauth-token access_token})) :key-fn keyword))
  (def runs (map (fn [m]
                                (assoc m
                                  :pace
                                  (Math/round (/ (:moving_time m) (* (:distance m) 0.00062137)))))
                              (filter #(= "Run" (:type %)) activities)))
  (def run_total_moving_time (reduce + (map #(:moving_time %) runs)))
  (def run_total_distace (reduce + (map #(:distance %) runs)))
  (def args {:athlete (:athlete @global_athlete)
             :sum_of_moving (reduce + (map #(:moving_time %) runs))
             :activities runs
             :average_pace (Math/round (double (/ 
                             (reduce + (map #(:pace %) runs))
                             (count runs))))
             :average_distance (* 0.00062137 (double (/
                                                     run_total_distace (count runs))))
             :total_average_pace (Math/round (double (/
                                                       run_total_moving_time
                                                       (* run_total_distace 0.00062137))))
             })
  {:body (render-file "activities.html" args)
   :headers {"Content-Type" "text/html"}})
