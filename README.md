# RunSum

## Motivation

I'm just playing around with the Strava API at http://labs.strava.com/developers/. This is mostly a proof of concept of a few different things. The biggest and easiest being connecting to Strava's API using clojure. I am also experimenting with namespaces and atoms.

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

### Features

* Displays a user's profile photo, and full name.
* Calculations taken from the most recent 30 activitites marked as runs:
* Total Moving Time
* Average Pace (calculated as the total moving time divided by total distance)
* Calculates average distance
* Displays activity stats

### Wishlist

* Compare paces from similar distances over elevation profiles.
* Add feature for toggling unit of meauser between scientific and imperial
* CSS Styling
